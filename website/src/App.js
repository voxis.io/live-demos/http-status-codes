import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link, useParams } from 'react-router-dom';
import { getAllData, getHTTPStatus } from './statuses';

import 'normalize.css';
import  './App.css';

const IS_STAGE_ENV = Boolean(location.host.match(/stage/i));
const VOXIS_LINK = <p>Sponsored by <a href="https://www.voxis.io/">Voxis &mdash; Code to Cloud in 60s</a></p>;
const RETURN_HOME_LINK = <p><Link to="/">&larr; Return to httpstatuscodes.dev</Link></p>;
const APP_TITLE = "HTTP Status Codes";
const APP_DESCRIPTION = <p><span>httpstatuscodes.dev</span> is an easy to use reference of HTTP Status Codes with definitions. Enter a status code or message in the search box or browse the list below.</p>;

export default function App() {
  const statuses = getAllData();

  document.title = (status ? `${status.code} ${status.title} | ${APP_TITLE}` : APP_TITLE);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <SectionListContainer statuses={statuses} />
        </Route>
        <Route path="/:codeId">
          <StatusTitleAndDescription />
        </Route>
      </Switch>
    </Router>
  );
}

const StatusTitleAndDescription = () => {
  const { codeId } = useParams();
  const status = getHTTPStatus(codeId);

  document.title = `${status.code} ${status.title} | ${APP_TITLE}`;

  return (
    <div>
      <Banner text={RETURN_HOME_LINK} />
      <article className="container">
        <h1><span>{status.code}</span> {status.title}</h1>
        <p>{status.description}</p>
      </article>
    </div>
  );
}

class Category extends React.Component {

  render() {
    const filterText = this.props.filterText;
    const statuses = [];

    if (this.props.category.categoryName.toLowerCase().includes(filterText)) {
      this.props.category.statuses.forEach((status) => {
        statuses.push(
          <li key={status.code}>
            <Link to={`/${status.code}`}>
              <span>{status.code}</span> {status.title}
            </Link>
          </li>
        );
      });
    } else {
      this.props.category.statuses.forEach((status) => {
        if (
          status.code.includes(filterText) ||
          status.title.toLowerCase().includes(filterText)
        ) {
          statuses.push(
            <li key={status.code}>
              <Link to={`/${status.code}`}>
                <span>{status.code}</span> {status.title}
              </Link>
            </li>
          );
        }
      });
    }

    return (
      <div className="container">
        <h2>{this.props.category.categoryName}</h2>
          <ul>
            {statuses}
          </ul>
      </div>
    );
  }

}

class SectionList extends React.Component {

  render() {
    const filterText = this.props.filterText.toLowerCase().trim();
    const categories = [];

    this.props.statuses.forEach((category) => {
      const found = category.statuses.find(
        status => status.code.includes(filterText) ||
        status.title.toLowerCase().includes(filterText)
      );
      if (found || category.categoryName.toLowerCase().includes(filterText)) {
        categories.push(
          <Category
            key={category.categoryName}
            category={category}
            filterText={filterText}
          />
        );
      }
    });

    return (
      <div>
        {categories}
      </div>
    );
  }

}

class SearchBar extends React.Component {

  constructor(props) {
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleEscKey = this.handleEscKey.bind(this);
    this.inputRef = React.createRef();
  }

  componentDidMount() {
    document.addEventListener(`keydown`, this.handleEscKey, false);
  }
  componentWillUnmount() {
    document.removeEventListener(`keydown`, this.handleEscKey, false);
  }

  handleFilterTextChange(e) {
    this.props.onFilterTextChange(e.target.value);
  }

  handleEscKey(e) {
    if(e.keyCode === 27) {
      this.props.onFilterTextChange(``);
    }
  }

  render() {
    return (
      <div className="container">
        <input
          type="text"
          ref={this.inputRef}
          autoFocus={(window.screen.width >= 400) ? true : false }
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
        />
      </div>
    );
  }

}

class Banner extends React.Component {

  render() {
    const text = this.props.text;
    return (
      <>
        {IS_STAGE_ENV && <div className="test-env container">Staging Environment</div>}
        <div className="banner container">{text}</div>
      </>
    );
  }

}

class SectionListContainer extends React.Component {

  constructor(props) {
    super(props)
    this.state = { filterText: `` };
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
  };

  handleFilterTextChange(filterText) {
    this.setState({
      filterText,
    });
  }

  render() {
    return (
      <div>
        <Banner text={VOXIS_LINK} />
        <div className="container">
          <h1 id="http-status-codes">{APP_TITLE}</h1>
          {APP_DESCRIPTION}
        </div>
        <SearchBar
          filterText={this.state.filterText}
          onFilterTextChange={this.handleFilterTextChange}
        />
        <SectionList
          statuses={this.props.statuses}
          filterText={this.state.filterText}
        />
      </div>
    );
  }

}
