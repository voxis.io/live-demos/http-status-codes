import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const mount = document.getElementById(`app`)
ReactDOM.render(<App />, mount);
