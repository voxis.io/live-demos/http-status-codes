# HTTP Status Codes
A small single page application that provides a reference for HTTP status codes.

## Quick Start

### Local Development
1. First `npm install`
2. Then `npm start`

### Deployment
The `master` branch gets deployed to httpstatuscodes.dev on push whilst the `stage` branch gets deployed to `stage.httpstatuscodes.dev`.

1. `git commit`
2. `git push`
